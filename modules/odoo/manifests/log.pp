class odoo::log(
  $log_path = '/var/log/'
){

  package{'logrotate':
    ensure => installed
  }

  file{'/etc/logrotate.d/odoo':
    ensure => present,
    content => template('odoo/logrotate.erb'),
    subscribe => Package['logrotate']
  }

}