class odoo::proxy(
  $hostname,
  $upstream_ports = [8000, 8001, 8002],
  $app_user,
  $certificate_path = undef,
  $certificate_key_path = undef
){

  validate_array($upstream_ports)
  validate_string($app_user)

  class{'nginx':}

  file {'/etc/nginx/sites-enabled/odoo':
      ensure => present,
      content => template('odoo/odoo.conf.erb'),
      notify => Service['nginx']
  }

  file{'/var/log/nginx/odoo/':
    ensure => directory,
    require => File['/etc/nginx/sites-enabled/odoo']
  }

}
