class odoo::service {

  file{'/etc/init/odoo.conf':
    ensure => file,
    content => template('odoo/start_service.conf.erb')
  }

  service {'odoo':
    enable => true,
    ensure => 'stopped',
    provider => 'upstart',
    subscribe => File['/etc/init/odoo.conf']
  }


}
