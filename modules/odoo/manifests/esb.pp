# $ca_cert_path should point to a fully qualified path on the host machine's file
# system where the Certificate Authority key is located

# $certificate_path should point to a full-qualified path on the host machine's
# file system where the SSL certificate is located (.pem)

# $keyfile_path should point to a full-qualified file path on the host machine's
# local file system

class odoo::esb (
  $host = '127.0.0.1',
  $port = '5672',
  $tls_port = '5671'
  $admin = true,
  $ca_cert_path,
  $certificate_path,
  $keyfile_path
){

  validate_string($host)
  validate_string($port)
  validate_port($string)

  # If we're going SSL - make sure all of the necessary variables are set
  if(is_string($ca_cert_path)){
    validate_string($tls_port)
    validate_string($certificate_path)
    validate_string($keyfile_path)
  }

  class { '::rabbitmq':
    service_manage    => false,
    port              => $port,
    delete_guest_user => true,
    admin_enable      => $admin
  }

}
