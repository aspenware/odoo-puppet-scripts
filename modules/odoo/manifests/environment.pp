class odoo::environment (
  $python_bin ='/usr/bin/python',
  $app_user,
  $app_dir,
  $app_entry,
  $config_path
){

  validate_string($python_bin)
  validate_string($app_user)
  validate_string($app_dir)
  validate_string($app_entry)
  validate_string($config_path)



  file{'/etc/environment.d':
    ensure => directory
  }

  file{'/etc/environment.d/odoo.sh':
    ensure  => present,
    content => template('odoo/env.sh.erb'),
    require => File['/etc/environment.d']
  }

}
