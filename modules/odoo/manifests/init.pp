class odoo{

  $python_packages= [
    'poppler-utils',
    'python-dateutil',
    'python-decorator',
    'python-docutils', # Possibly a dev package?
    'python-feedparser',
    'python-gdata',
    'python-imaging',
    'python-jinja2',
    'python-ldap',
    'python-libxslt1',
    'python-lxml',
    'python-mako',
    'python-mock',
    'python-openid',
    'python-passlib',
    'python-pdftools',
    'python-psutil',
    'python-psycopg2',
    'python-pybabel',
    'python-pychart',
    'python-pydot',
    'python-pypdf',
    'python-pyparsing',
    'python-reportlab',
    'python-requests',
    'python-simplejson',
    'python-tz',
    'python-unittest2', # Dev package?
    'python-vatnumber',
    'python-vobject',
    'python-webdav',
    'python-werkzeug', # Dev package?
    'python-xlwt',
    'python-yaml',
    'python-zsi'
  ]

  $system_packages = [
    'upstart',
    'python',
    'wget'
  ]


  package {$python_packages: ensure => installed}
  package {$system_packages: ensure => installed }

}