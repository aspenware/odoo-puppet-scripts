class odoo::user(
  $name = "odoo",
  $group = $name,
  $home = undef
  ){

    validate_string($password)

    if(empty($home)){
      $user_home = "/home/${name}/"
    }
    else
    {
      validate_string($home)
      $user_home = $home
    }

    user{'odoo application user':
      name        => $name,
      ensure      => present,
      comment     => 'Odoo application user',
      expiry      => absent,
      home        => $home,
      shell      => '/bin/bash',
      managehome  => true,
      password => '*'
    }
}
