# == class: odoo::database
#
# Used to create and configure the PostgreSQL database for Odoo installations
#
# === Parameters
#
# [*server_is_open*]
#   Boolean value to represent whether or not to allow remote machines to connect to the PostgreSQL installation
#   See examples below.
#
# [*database_name*]
#   String value that sets the name of the database to be created for the Odoo application to use
#   By default, it is set to 'odoo_db'
#
# [*database_port*]
#   Integer value that sets port number to bind the PostgreSQL server instances to
#   Defaults to 4532
#
# [*database_encoding*]
#   String value to set the string encoding type for the Database used by Odoo.
#   Defaults to 'utf8'
#
# [*database_owner*]
#   String value to set the role name for the owner of the database
#   Defaults to 'odoo_user'
#
# [*database_owner_password*]
#   String value to sets the password for the database owner
#
# === Examples
#
# Provide some examples on how to use this type:
#
# Using the default values:
#
#   class{'odoo::database':
#     database_password => 'My$ecretP@$Sw0rd'
#   }
#
# Overriding the default values:
#
#   class{'odoo::database':
#     server_is_open => true,
#     database_name => 'skynet',
#     database_port => 5542,
#     database_owner => 'John Connor'
#     database_password => 'JudgementDay'
#   }
#
# === Authors
# Michael Filbin, m.filbin@aspenware.com
# === Copyright


class odoo::database (
  $server_is_open = false,
  $server_port= 4532,
  $database_name = 'odoo_db',
  $database_encoding = 'utf8',
  $database_owner = 'odoo_user',
  $database_owner_password
  )
  {

    validate_string($database_encoding)
    validate_string($database_name)
    validate_string($database_owner)
    validate_string($database_owner_password)
    validate_bool($server_is_open)

    if $server_is_open {
      $listen_address = '*'
    }
    else {
      $listen_address = 'localhost'
    }

  # Sets up the PostgreSQL database
  # For additional configuration options - see https://forge.puppetlabs.com/puppetlabs/postgresql

  class { 'postgresql::server':
    listen_addresses    => $listen_address,
    port                => $server_port,
    postgres_password   => $database_owner_password
  } -> postgresql::server::role{ $database_owner :
    password_hash   => postgresql_password($database_owner, $database_owner_password),
    superuser       => true
  } -> postgresql::server::db { $database_name :
    user              => $database_owner,
    password          => $database_owner_password,
    encoding          => $database_encoding,
    owner             => $database_owner,
  }

  if $server_is_open {
    postgresql::server::pg_hba_rule { 'allow application network to access app database':
      description => 'Open up postgresql for access from VM host',
      type        => 'host',
      database    => $database_name,
      user        => $database_owner,
      address     => 'all',
      auth_method => 'md5'
    }
  }

}
