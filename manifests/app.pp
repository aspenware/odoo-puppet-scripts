include apt

$app_user = 'odoo'

class {'odoo':
} -> class{'odoo::environment':
  python_bin => '/usr/bin/python',
  app_user => $app_user,
  app_dir => "/home/${app_user}/current/",
  app_entry => 'openerp-server',
  config_path => 'openerp-server.conf'
} -> class{'odoo::user':
  name => $app_user,
  require => Class['odoo::environment'],
} -> class{'odoo::service':
  require => Class['odoo::user']
}

class{'odoo::proxy':
  upstream_ports => [8046],
  app_user => 'odoo',
  hostname => $::hostname
}

class{'odoo::log':
  log_path => '/var/log/nginx/odoo/'
}
